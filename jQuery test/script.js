window.onload = function() {
    var cars = {
          "Maruti": [
              ["800", "Alto", "WagonR", "Esteem","SX4"], 
              [1995, 2000, 2002, 2004, 2007]],
          "TATA": [
              ["Indica", "Indigo", "Safari", "Sumo"], 
              [2001, 2006, 2003, 2001]],
          "Chevrolet": [
              ["Beat", "Travera", "Spark"], 
              [2006, 2002, 2007]],
          "Toyota": [
              ["Camary", "Etios", "Corolla", "Endevour"], 
              [2005, 2010, 2003, 2008]]
        }
      
    make_select = document.querySelector('#make'),
    model_select = document.querySelector('#model');
    year_select = document.querySelector('#year');
  
    
    setOptions(make_select, Object.keys(cars));
    setOptions(model_select, cars[make_select.value][0]);
    
    make_select.addEventListener('change', function() {
        setOptions(model_select,cars[make_select.value][0]);
        var make = make_select.value;
        model_select.addEventListener('change', function() {
            var model = model_select.value;
            var index = cars[make][0].indexOf(model);
            var manufYear = cars[make][1][index];
            let yearOptions = [];
            let currYear = (new Date()).getFullYear();
            for(let i=manufYear; i< currYear+1; i++){
                yearOptions.push(i);
            }
            setOptions(year_select, yearOptions);
        });
    });
  
    function setOptions(dropDown, options) { 
      dropDown.innerHTML = '';
      options.forEach(function(value) {
        dropDown.innerHTML += '<option value="' + value + '">' + value + '</option>';
      });
    } 

    function myFunction() {
        var x = document.getElementById("myForm").action;
        document.getElementById("formEntries").innerHTML = x;
      }

    
  };