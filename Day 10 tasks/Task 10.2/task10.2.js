function findSum(arr){
    sortedArr = arr.sort(function(a,b){return a-b});
    console.log("sort" +sortedArr);
    var sum = 0;
    var minSum  = 9999999;
    var n = sortedArr.length;
    if(n<0)
        return;
    var arrayPair = [];
    var l = 0;
    var r = n-1;
    while(l<r){
        sum = sortedArr[l] + sortedArr[r];
        if(Math.abs(sum) <= Math.abs(minSum)){
            minSum = sum;
            arrayPair.push([sortedArr[l], sortedArr[r], sum, minSum]);
        }
        if(sum >= 0){
            r--;
        }
        if(sum < 0){
            l++;
        }    
    }
    console.log(arrayPair);  
    return arrayPair;
}
function findSumZero(arrayPair){
    for(let i=0; i<arrayPair.length; i++){
        var leastAbsoluteSum = Math.abs(Math.min(arrayPair[i][2]));
    }
    for(let i=0; i<arrayPair.length; i++){
        if((Math.abs(arrayPair[i][2])) == leastAbsoluteSum){
            console.log("Pair of elements with sum closest to zero are: " ,arrayPair[i][0] + " " + arrayPair[i][1]);
        }
    }
}

var input = prompt("Enter numbers (comma separated): ");
var inputArray = input.split(',');
var numArray = [];
for(var i in inputArray)
{
	if(isNaN(inputArray[i])==false && inputArray[i]!="" && inputArray[i]!=" ")
    {
    	numArray.push(Number(inputArray[i]));
    }
}
console.log(numArray);
var arr = findSum(numArray); 
findSumZero(arr);
