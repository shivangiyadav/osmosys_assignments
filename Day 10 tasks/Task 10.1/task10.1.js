function charFreq(str){
    var freq = {};
    for (var i=0; i< str.length; i++){
        var char = str.charAt(i);
        if(freq[char]== undefined){
            freq[char] = 1;
        }
        else{
            freq[char]++;
        }  
    }
    console.log(freq);
}
var str = prompt("Enter string: ");
charFreq(str);