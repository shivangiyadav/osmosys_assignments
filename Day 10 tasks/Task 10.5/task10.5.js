var itCompanies = {
    "TCS": {
        revenue: 10000000,
        expenses: {
            "salaries": 30, //30, 20 and 15 are perecentages of total revenue
            "rent": 20,
            "utilities": 15
        },
        "employees":[
            {
                "name": "Joe",
                "age": 30,
                "role": "Admin"
            },
            {
                "name": "Dave",
                "age": 34,
                "role": "Tester"
            },
            {
                "name": "Sherlock",
                "age": 45,
                "role": "Programmer"
            }
        ]
    },

    "Infosys": {
        revenue: 9750000,
        expenses: {
            "salaries": 35, //30, 20 and 15 are perecentages of total revenue
            "rent": 20,
            "utilities": 15
        },
        "employees":[
            {
                "name": "James",
                "age": 31,
                "role": "Admin"
            },
            {
                "name": "Ken",
                "age": 26,
                "role": "Tester"
            },
            {
                "name": "Sheila",
                "age": 42,
                "role": "Programmer"
            }
        ]
    },

    "Wipro": {
        revenue: 9780000,
        expenses: {
            "salaries": 30, //30, 20 and 15 are perecentages of total revenue
            "rent": 25,
            "utilities": 10
        },
        "employees":[
            {
                "name": "Jenny",
                "age": 29,
                "role": "Admin"
            },
            {
                "name": "Reva",
                "age": 37,
                "role": "Tester"
            },
            {
                "name": "Mark",
                "age": 43,
                "role": "Programmer"
            }
        ]
    }
}
function getEmployees(){
    let emp = [];
    for(let comp in itCompanies){
        itCompanies[comp].employees.forEach(Element => {
            emp.push(Element);
        })
    }
    return emp;
}
function ageLessThanEqualTo(ageThreshold){
    let emp = getEmployees();
    let empAgeLessThan = emp.filter(employee => employee["age"] <= ageThreshold);
    empAgeLessThan.forEach(e => {
        console.log(`${e.name} has age less than or equal to ${ageThreshold} years`);
    })
}
function ageGreaterThan(ageThreshold){
    let emp = getEmployees();
    let empAgeGreaterThan = emp.filter(employee => employee["age"] > ageThreshold);
    empAgeGreaterThan.forEach(e => {
        console.log(`${e.name} has age greater than ${ageThreshold} years`);
    })
}
function youngestAndOldest(){
    let emp = getEmployees();
    let sortedEmployees = emp.sort((a,b) => a.age - b.age);
    console.log(sortedEmployees);
    let youngestEmp = sortedEmployees[0];
    let oldestEmp = sortedEmployees[sortedEmployees.length-1];
    let resObj ={
        youngestEmployee : youngestEmp,
        oldestEmployee : oldestEmp
    }
    console.log("Youngest employee is", resObj.youngestEmployee);
    console.log("Oldest employee is", resObj.oldestEmployee);
}
function displayEmpRoles(){
    let emp = getEmployees();
    let admins = emp.filter(e => e.role === "Admin");
    let testers = emp.filter(e => e.role === "Tester");
    let programmers = emp.filter(e => e.role === "Programmer");

    console.log("Admins: ", admins);
    console.log("Testers: ", testers);
    console.log("Programmers: ", programmers);
    
}
function displayRevenueAndProfit(){
    let rev = [];
    let profits = [];
    let expPercent=0;
    for(let comp in itCompanies){
        rev.push(itCompanies[comp].revenue);
        for(expense in itCompanies[comp].expenses){
            expPercent += itCompanies[comp].expenses[expense];  
        }
        profits.push([comp, (itCompanies[comp].revenue)*((100-expPercent)/100)]);
        console.log(`${comp} earns a profit of Rs. ${(itCompanies[comp].revenue)*((100-expPercent)/100)}`);
    }
    profitSorted = profits.sort(function(a,b){
        return a[1]-b[1];
    });
    console.log("Max profit: ", profitSorted[profitSorted.length-1]);
    return rev;
    return profits;
}

var ageThreshold = prompt("Enter age threshold for comparison: ");
ageLessThanEqualTo(ageThreshold);
ageGreaterThan(ageThreshold);
youngestAndOldest();
displayEmpRoles();
displayRevenueAndProfit();

