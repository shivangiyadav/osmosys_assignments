CREATE DATABASE  IF NOT EXISTS `res_productions` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `res_productions`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: res_productions
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author_details`
--

DROP TABLE IF EXISTS `author_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author_details` (
  `author_id` int NOT NULL AUTO_INCREMENT,
  `author_name` varchar(45) NOT NULL,
  `DOB` date NOT NULL,
  `description` varchar(200) NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains details of authors';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_details`
--

LOCK TABLES `author_details` WRITE;
/*!40000 ALTER TABLE `author_details` DISABLE KEYS */;
INSERT INTO `author_details` VALUES (1,'Haruki Murakami','1949-01-12','Haruki Murakami is a Japanese writer. His books and stories have been bestsellers in Japan as well as internationally.','2020-06-20','Shivangi',NULL,NULL,1),(2,'SALLY ROONEY','1994-03-18','loren ipsum','2020-06-20','Shivangi',NULL,NULL,1);
/*!40000 ALTER TABLE `author_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_details`
--

DROP TABLE IF EXISTS `book_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_details` (
  `book_id` int NOT NULL AUTO_INCREMENT,
  `book_title` varchar(45) NOT NULL,
  `book_author` varchar(45) NOT NULL,
  `book_publisher` varchar(45) NOT NULL,
  `genre` varchar(45) NOT NULL,
  `release_year` int NOT NULL,
  `rating` int NOT NULL,
  `author_id` varchar(45) NOT NULL,
  `publisher_id` varchar(45) NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains details of all books';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_details`
--

LOCK TABLES `book_details` WRITE;
/*!40000 ALTER TABLE `book_details` DISABLE KEYS */;
INSERT INTO `book_details` VALUES (1,'NORMAL PEOPLE','SALLY ROONEY','Faber and Faber','DRAMA',2018,3,'4','3','2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `book_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friend_list`
--

DROP TABLE IF EXISTS `friend_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friend_list` (
  `user_id` int NOT NULL,
  `friend_id` int NOT NULL,
  `friend_name` varchar(45) NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`friend_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains list of friends';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friend_list`
--

LOCK TABLES `friend_list` WRITE;
/*!40000 ALTER TABLE `friend_list` DISABLE KEYS */;
INSERT INTO `friend_list` VALUES (1,2,'MARK','2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `friend_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres_available`
--

DROP TABLE IF EXISTS `genres_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genres_available` (
  `genre_id` int NOT NULL AUTO_INCREMENT,
  `genre` varchar(45) NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`genre_id`),
  UNIQUE KEY `genre_UNIQUE` (`genre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains genres of books available';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres_available`
--

LOCK TABLES `genres_available` WRITE;
/*!40000 ALTER TABLE `genres_available` DISABLE KEYS */;
INSERT INTO `genres_available` VALUES (1,'FANTASY','2020-06-20','SHIVANGI',NULL,NULL,1),(2,'MYSTERY','2020-06-20','SHIVANGI',NULL,NULL,1),(3,'HORROR','2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `genres_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liked_books`
--

DROP TABLE IF EXISTS `liked_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `liked_books` (
  `book_id` int NOT NULL,
  `user_id` int NOT NULL,
  `book_title` varchar(45) NOT NULL,
  `liked_on` date NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`book_id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `book_id` FOREIGN KEY (`book_id`) REFERENCES `book_details` (`book_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains books liked by users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liked_books`
--

LOCK TABLES `liked_books` WRITE;
/*!40000 ALTER TABLE `liked_books` DISABLE KEYS */;
INSERT INTO `liked_books` VALUES (1,1,'NORMAL PEOPLE','2020-06-20','2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `liked_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher_details`
--

DROP TABLE IF EXISTS `publisher_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publisher_details` (
  `publisher_id` int NOT NULL AUTO_INCREMENT,
  `publisher_name` varchar(45) NOT NULL,
  `established_on` date NOT NULL,
  `address` varchar(80) NOT NULL,
  `description` varchar(70) NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`publisher_id`),
  UNIQUE KEY `publisher_name_UNIQUE` (`publisher_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains details of publishers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_details`
--

LOCK TABLES `publisher_details` WRITE;
/*!40000 ALTER TABLE `publisher_details` DISABLE KEYS */;
INSERT INTO `publisher_details` VALUES (1,'ABC','1999-10-23','12, STR 4, NY','loren ipsum','2020-06-20','SHIVANGI',NULL,NULL,1),(2,'XYZ','1997-12-02','14, STR 4, NY','loren ipsum','2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `publisher_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental_records`
--

DROP TABLE IF EXISTS `rental_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rental_records` (
  `record_id` int NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) NOT NULL,
  `book_id` varchar(45) NOT NULL,
  `rental_date` date NOT NULL,
  `return_date` date NOT NULL,
  `late_return_fee` int DEFAULT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains rental records of all users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental_records`
--

LOCK TABLES `rental_records` WRITE;
/*!40000 ALTER TABLE `rental_records` DISABLE KEYS */;
INSERT INTO `rental_records` VALUES (1,'1','1','2020-06-20','2020-07-20',0,'2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `rental_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_details` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `gender` enum('male','female','other') NOT NULL,
  `DOB` date NOT NULL,
  `user_category` enum('Normal','Premium') NOT NULL,
  `address` varchar(80) NOT NULL,
  `phone_number` int NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains the registration details of the users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES (1,'DAVE','HAN','male','1999-03-23','Normal','13, STR-7, NY',1234567,'2020-06-20','SHIVANGI',NULL,NULL,1),(2,'MARK','FRANC','male','1999-08-21','Normal','11, STR-7, NY',1234567,'2020-06-20','SHIVANGI',NULL,NULL,1),(3,'MARTHA','LAW','female','1997-04-21','Normal','10, STR-7, NY',12345678,'2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wishlist` (
  `wishlist_id` int NOT NULL,
  `user_id` int NOT NULL,
  `book_title` varchar(45) NOT NULL,
  `wishlisted_on` date NOT NULL,
  `created_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`wishlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='contains wishlisted books';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist`
--

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
INSERT INTO `wishlist` VALUES (1,1,'NORMAL PEOPLE','2020-06-20','2020-06-20','SHIVANGI',NULL,NULL,1);
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 14:27:25
