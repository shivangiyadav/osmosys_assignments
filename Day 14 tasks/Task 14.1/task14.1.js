//closure
function birthdays(){
    //check browser support
    if(typeof(Storage) !== "undefined"){
        //store
        localStorage.setItem("Sneha", new Date(1998, 2, 12));
        localStorage.setItem("Ramaa", new Date(1997, 6, 23));
        localStorage.setItem("Harshit", new Date(1998, 5, 21));
        localStorage.setItem("Yukti", new Date(1997, 6, 23));
        localStorage.setItem("Mehul", new Date(1999, 8, 15));
        localStorage.setItem("Parul", new Date(1996, 10, 2));
    }  
    function getAllBday(){
        let bdayList = [];
        for( let i = 0; i < localStorage.length; i++){
            bdayList.push([localStorage.key(i), localStorage.getItem(localStorage.key(i))]);
        }
        console.log(bdayList);
        return bdayList;
    }
    return getAllBday();
}


var get_func_getAllBday = birthdays();
function getNextMonthBday(get_func_getAllBday){
    for(var i=0; i<get_func_getAllBday.length; i++){
        let allBday = new Date(get_func_getAllBday[i][1]);
        if((new Date()).getMonth() == allBday.getMonth()){
            console.log(get_func_getAllBday[i][0] + " " + "has birthday next month!")
        }
    }
}
function getSundayBday(get_func_getAllBday){
    for(var i=0; i<get_func_getAllBday.length; i++){
        let allBday = new Date(get_func_getAllBday[i][1]);
        if((0 == allBday.getDay() )){
            console.log(get_func_getAllBday[i][0] + " " + "has birthday on a Sunday!")
        }
    }
}
function sameDayBday(get_func_getAllBday){
    for(var i=0; i<get_func_getAllBday.length; i++){
        let allBday1 = new Date(get_func_getAllBday[i][1]);
        for(var j=i+1 ; j<get_func_getAllBday.length; j++){
            let allBday2 = new Date(get_func_getAllBday[j][1]);
            if((allBday1.getTime() == allBday2.getTime()) && get_func_getAllBday[i] != get_func_getAllBday[j] ){
                console.log(get_func_getAllBday[i][0] + " " + "and " + get_func_getAllBday[j][0] + " " + "have birthday on same date!")
            }
        }
        
    }
}
function upcomingBday(get_func_getAllBday){
    let days = [];
    for(let i=0; i<get_func_getAllBday.length; i++){
        let allBday = new Date(get_func_getAllBday[i][1]);
        let currentYearBday = new Date((new Date).getFullYear(), allBday.getMonth(), allBday.getDate());
        if((new Date).getTime() > currentYearBday.getTime()){
            currentYearBday.setFullYear(currentYearBday.getFullYear()+1);
        }
        let diff = currentYearBday.getTime() - (new Date).getTime();
        days.push([get_func_getAllBday[i][0], Math.floor(diff/(1000*60*60*24))]); 
    }
    let sortedDays = days.sort(function(a,b){
        return a[1] - b[1]
    });
    for(let i = 0; i<sortedDays.length; i++){
        if(Math.min(sortedDays[i][1])){
            console.log(days[i][0] + "'s" + " birthday upcoming in " + sortedDays[i][1] + " days");
        }
    }
}
function displayOldestFriend(get_func_getAllBday){
    let diff = [];
    for(var i=0; i<get_func_getAllBday.length; i++){
        let allBday = Date.parse(get_func_getAllBday[i][1]);
        diff.push([get_func_getAllBday[i][0], ((new Date()).getTime() - allBday)]);
    }
    let sortedDiff = diff.sort(function(a,b){
        return a[1] - b[1]
    });
    console.log(sortedDiff[sortedDiff.length -1 ][0] + " is the oldest!"); 
}
function addFriendBday(get_func_getAllBday){
    let name = window.prompt("Enter friend's name: ");
    let bday = new Date(window.prompt("Enter friend's birthday (yyyy, mm, dd): "));
    localStorage.setItem(name, bday);
    console.log("New friend's birthday added to local storage, refresh local storage!")
}


getNextMonthBday(get_func_getAllBday);
getSundayBday(get_func_getAllBday);
sameDayBday(get_func_getAllBday);
upcomingBday(get_func_getAllBday);
displayOldestFriend(get_func_getAllBday);
addFriendBday(get_func_getAllBday);