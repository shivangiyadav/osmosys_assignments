function fileTypes(){
    var mixedUpFiles = [
        {
            "filename" : "flower",
            "type" : "jpg"
        },
        {
            "filename" : "family-video-clip",
            "type" : "mp4"
        },
        {
            "filename" : "phone-ringtone",
            "type" : "mp3"
        },
        "javascript-exercises.txt",
        "learning-html-basics.rtf",
        {
            "filename" : "friend-video-clip",
            "type" : "mp4"
        },
        {
            "filename" : "resume",
            "type" : "docx"
        },
        {
            "filename" : "student-report",
            "type" : "csv"
        },
        {
            "filename" : "sms-ringtone",
            "type" : "mp3"
        },
        "html-basics.pdf",
        "dubsmash.mp4",
        "screenshot.png",
        {
            "filename" : "faculty-report",
            "type" : "xlsx"
        },
        {
            "filename" : "puppy",
            "type" : "svg"
        },
    ]
    
    var segregatedFiles = {
        "audio": [],
        "video": [],
        "document": [],
        "image": []
    }
    
    var imgTypes = ["svg", "jpg", "png"];
    var docTypes = ["txt", "pdf", "csv", "rtf", "xlsx", "docx"];
    var audioTypes = ["mp3"];
    var videoTypes = ["mp4"];
    var types = [["image", imgTypes], ["document", docTypes], ["audio", audioTypes], ["video",videoTypes]];
    
    for(t = 0; t < types.length; t++){
        for(i = 0; i < mixedUpFiles.length; i++){
            for(j =0; j < types[t][1].length; j++){
                
                if(mixedUpFiles[i].type == types[t][1][j]){
                    segregatedFiles[types[t][0]].push(mixedUpFiles[i].filename);   
                }
                reg = new RegExp(types[t][1][j]);
                if(reg.test(mixedUpFiles[i])){
                    var fileDetails =  mixedUpFiles[i].split(/\./);
                    segregatedFiles[types[t][0]].push(fileDetails[0]);
                }
            }
        }
    }
    return segregatedFiles;
}


var segregatedFiles = fileTypes();
console.log(segregatedFiles);



    
